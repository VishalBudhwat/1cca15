package Inheritance;

public class MainApp3 {
    public static void main(String[] args) {
        MiMobile m1= new MiMobile();
        RealmeMobile r1= new RealmeMobile();
        m1.test(20000,4);
        m1.getDetails("Mi");
        System.out.println("=======================");
        r1.test(30000,6);
        r1.getDetails("Realme");
    }
}
