package Inheritance;

public class Mobile {
    void test(double price, int ram){
        System.out.println("Price of Mobile "+price);
        System.out.println("Ram of Mobile "+ram);
    }
}
