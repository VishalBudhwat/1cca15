package array;

import java.util.Scanner;

public class Demo8 {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        System.out.println("Enter total no of Floors");
        int floors= sc1.nextInt();

        System.out.println("Enter Total no flats on each floor");
        int flats= sc1.nextInt();

        System.out.println("Enter all flats");
        int[][]data= new int[floors][flats];

        // for accepting value
        for(int i=0;i<floors;i++){
            for(int j=0;j<flats;j++){
                data[i][j]= sc1.nextInt();
            }
        }
        // for printing value
        for(int i=0;i<floors;i++){
            System.out.println("floor no"+(i+1));
            System.out.println("===================");
            for(int j=0;j<flats;j++){
                System.out.print("flats no " +data[i][j]+"\t");
            }
            System.out.println();
            System.out.println();
        }
    }
}
