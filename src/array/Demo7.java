package array;

import java.util.Scanner;

public class Demo7 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Total No. of Floors:");
        int floors = sc1.nextInt();

        System.out.println("Enter total No. Flats on each Floor:");
        int flats= sc1.nextInt();

        int[][]data= new int[floors][flats];
        System.out.println("Enter "+(floors*flats)+" Flats");
        // accept value from end user
        for(int a=0;a<floors;a++){
            for(int b=0;b<flats;b++){
                data[a][b] = sc1.nextInt();
            }
        }
        //Print value
        for(int a=0;a<floors;a++){
            System.out.println("Floor "+(a+1));
            for(int b=0;b<flats;b++){
                System.out.print("Flat no "+data[a][b]+"\t");
            }
            System.out.println();
            System.out.println();
        }
    }
}
