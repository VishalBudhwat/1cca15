package Array1;
//reverse the array using the single array
public class ReverseArray {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5,7, 6};
        int count = arr.length - 1;
        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[count];
            arr[count] = temp;
            count--;

        }
        //for printing the value
        for (int i : arr)
        {
            System.out.print(i);
        }
    }
}