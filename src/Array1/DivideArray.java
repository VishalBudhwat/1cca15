package Array1;
//divide the array and reverse the array
public class DivideArray {
    public static void main(String[] args) {
        int[]arr={1,2,3,4,5,6};

        int count= arr.length/2;
        for(int i=0;i<arr.length/2;i++){
            int temp=arr[i];
            arr[i]=arr[count];
            arr[count]=temp;
            count++;
        }
        for(int i:arr){
            System.out.print(i); //4,5,6,1,2,3
        }
    }
}
