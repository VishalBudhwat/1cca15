package String;

public class Demo3 {
    public static void main(String[] args) {
        String s1= new String("Pune");
        System.out.println(s1);
        String s2= "Mumbai";
        System.out.println(s2);
        char[] data=s2.toCharArray();
        for(int i=0;i<data.length;i++){
            System.out.println(data[i]);
        }
        System.out.println("==========");
        char[] data1= s1.toCharArray();
        for(int i=data1.length-1;i>=0;i--){
            System.out.println(data1[i]);
        }
        System.out.println("==========");
        String s3= data1.toString();
        System.out.println(data1);
    }
}
