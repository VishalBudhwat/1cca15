package forloop;

public class Demo {
    public static void main(String[] args) {
        System.out.println("Program is started");
        for (int a = 1; a <= 10; a++) {
            System.out.println(a);

        }
        System.out.println("Program is ended");
    }
}
