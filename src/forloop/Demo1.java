package forloop;

import java.util.Scanner;
public class Demo1 {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        System.out.println("Enter Start Point");
        int a= sc1.nextInt();
        System.out.println("Enter End Point");
        int b= sc1.nextInt();
        for ( int c=a; c<=b; c++) {
            if(c%2==0) {
                System.out.println( c);
            }

        }
    }

}
