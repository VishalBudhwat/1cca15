package forloop;

public class Demo5 {
    public static void main(String[] args) {
        int [] arr1= {12,25,56};
        int[] arr2= {58,98,12};
        int [] sum= new int[arr1.length];
        for( int a=0; a<arr1.length;a++){
            sum [a]= arr1[a]+arr2[a];
            System.out.println(sum[a]);
        }
    }
}
