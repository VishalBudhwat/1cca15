package forloop;
import java.util.Scanner;
public class Demo2 {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        System.out.println("Enter Start Point");
        int start= sc1.nextInt();

        System.out.println("Enter End Point");
        int end= sc1.nextInt();

        for(int a=start;a<=end;a++){
            if(a%2==0)
            {int b= a*a;
                System.out.println(b);}
        }

    }
}
