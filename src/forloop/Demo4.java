package forloop;

public class Demo4 {
    public static void main(String[] args) {
        int col=1;
        int row=3;

        for(int i=1;i<=row; i++)
        {
            for(int j=1;j<=col;j++)
            {
                System.out.print(" * ");
            }
            System.out.println();
            col++;
    }
}}
