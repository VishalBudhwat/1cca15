package Series;
//spy means addition of each digit and multiplication of each digit are same;
public class Spy_no_range {
    public static void main(String[] args) {
        for (int i = 10; i <=10000; i++) {
            int a = i;
            int sum = 0;
            int mul = 1;
            while (a != 0) {
                int r = a % 10;
                sum = sum + r;
                mul = mul * r;
                a /= 10;
            }
            if (sum == mul)
                System.out.println(i);
        }
    }
}