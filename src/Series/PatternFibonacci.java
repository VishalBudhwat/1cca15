package Series;

public class PatternFibonacci {
    public static void main(String[] args) {
        int line=4;
        int star=1;

        for(int i=0;i<line;i++)
        {
            int n1=0, n2=1;
            for(int j=0;j<star;j++)
            {
                System.out.print(" "+n1+" ");
                int add=n1+n2;
                n1=n2;
                n2=add;
            }
            System.out.println();
            star=star+2;
        }
    }
}
