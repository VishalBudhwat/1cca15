package Programs;

public class Demo16 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;

        for (int i=0; i<line; i++) {
            int ch1=1;
            for (int j=0; j<star; j++) {
                if(j%2!=0)
                    System.out.print(" * ");
                else
                    System.out.print(" "+ch1++ +" ");


            }
            System.out.println();


        }
    }
}
