package Programs;

public class Demo19 {
    public static void main(String[] args) {
        int line =5;
        int star=4;
        int ch1=1;
        for (int i=0;i<line;i++){
            int ch2=ch1;
            for(int j=0;j<star;j++){
                if(i%2!=0) {
                    System.out.print(" * ");
                }
                else{
                    System.out.print(" "+ch2+" ");
                }
            }
            System.out.println( );
            if(i%2==0){
                ch1++;
            }
        }
    }
}
