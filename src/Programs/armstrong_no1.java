package Programs;
//armstrong means sum of each digit power of the given number is equal to that no.
public class armstrong_no1 {
    public static void main(String[] args) {
        for (int i = 10; i <= 10000; i++) {
            int a = i;
            int temp = a;
            double sum = 0;
            int count = 0;

            while (a != 0) {
                a /= 10;
                count++;
            }
            while (temp != 0) {
                int r = temp % 10;
                sum = sum + Math.pow(r, count);
                temp /= 10;
            }
            if (sum == i) {
                System.out.println(i);
            }
        }
    }
}
