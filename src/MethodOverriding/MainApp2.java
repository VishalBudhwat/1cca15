package MethodOverriding;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        Amazon a1= new Amazon();
        Flipkart f1= new Flipkart();

        System.out.println("Select Ecommerce Platform");
        System.out.println("1.Amazon\n2.Flipkart");
        int ch= sc1.nextInt();
        System.out.println("Enter qty");
        int qty= sc1.nextInt();
        System.out.println("Enter Price");
        double price= sc1.nextDouble();

        if(ch==1)
        {
            a1.sellProduct(qty, price);
        }
        else if(ch==2)
        {
            f1.sellProduct(qty,price);
        }
        else
        {
            System.out.println("Invalid Choice");
        }

    }
}
