package MethodOverriding;

public class Flipkart {
    void sellProduct(int qty, double price){
        double total= qty*price;
        double finalAmt= total-total*0.1;
        System.out.println("final Amount "+finalAmt);
    }
}
